use std::{collections::HashMap, io::Write};
use makina_lib::VM;
use makina_lib::INST_HALT_VM;
use makina_lib::ExecutorST;
use makina_lib::NiceDefaults;

enum HandleAsm {
    UnresolvedJMP(u64,String),
    Other(u64),
}

fn get_num_reg_from_asm(s: &str) -> u8 {
    return match s {
        "a" => 0,
        "b" => 1,
        "c" => 2,
        "d" => 3,
        "e" => 4,
        "f" => 5,
        "g" => 6,
        "h" => 7,
        _ => {
            eprintln!("Error: unknown register {}.", s);
            std::process::exit(1);
        }
    }
}

fn asm_to_bytecode(s: String, included: &mut Vec<String>) -> Vec<u8> {
    let avail_regs = ["a","b","c","d","e","f","g","h"];
    let mut labels: HashMap<String, usize> = HashMap::new();
    let mut constants: HashMap<String,usize> = HashMap::new();
    let mut inter_insts: Vec<HandleAsm> = Vec::new();
    let mut ptr_base = 0u32;
    let mut ret = Vec::new();
    for inst in s.lines() {
        let words : Vec<&str> = inst.split_whitespace().collect();
        if words.len() < 1 {
            continue;
        }
        match words[0] {
            "include" => {
                // Includes another .mknasm file, following the **Library Convention**
                // The library convention is basically a label at the end of the library and a jump
                // at the beggining of the library to that label, in order to not run code.
                // Note that this isn't enforced, so libaries can execute arbritary code.
                if included.contains(&words[1].to_owned()) {
                    continue;
                } else {
                    included.push(words[1].to_owned());
                    let asm = read_asm_file(words[1]);
                    let mut new_s = String::new();
                    for inst in s.lines() {
                        if inst.split_whitespace().collect::<Vec<&str>>() != words {
                            new_s += inst;
                            new_s += "\n";
                        }
                    }
                    new_s += &asm;
                    //println!("{}", &new_s);
                    return asm_to_bytecode(new_s, included); 
                }
            }
            "byte" => {
                if let Ok(val) = words[2].parse::<u8>() {
                    constants.insert(words[1].to_owned(), val as usize);
                } else {
                    eprintln!("{} is not byte-sized!", words[2]);
                    std::process::exit(1);
                }
            }
            "byte*" => {
                let size_alloc = words.len() - 2;
                let name = words[1].to_owned();
                constants.insert(name, ptr_base as usize);
                inter_insts.push(HandleAsm::Other(size_alloc as u64)); // li a <size>
                inter_insts.push(HandleAsm::Other(((get_num_reg_from_asm("b") as u64) << 54) + ptr_base as u64)); // li b <new_ptr>
                inter_insts.push(HandleAsm::Other(126100789566373898)); // li h 10
                inter_insts.push(HandleAsm::Other(1873497444986126336)); // ncall
                inter_insts.push(HandleAsm::Other((5 << 61) + (3 << 58) + ((get_num_reg_from_asm("c") as u64) << 51))); // raw c a
                ptr_base += 1;
                for i in 2..words.len() {
                    if let Ok(bite) = words[i].parse::<u8>() {
                        inter_insts.push(HandleAsm::Other(18014398509481984 + bite as u64)); // li b <val>
                        inter_insts.push(HandleAsm::Other(758856537211928576)); // sb c b
                        inter_insts.push(HandleAsm::Other(2490490593935884289)); // addi c c 1
                    } else {
                        eprintln!("{} is not byte-sized!", words[i]);
                        std::process::exit(1);
                    }
                }

            }
            "quarter" => {
                if let Ok(val) = words[2].parse::<u16>() {
                    constants.insert(words[1].to_owned(), val as usize);
                } else {
                    eprintln!("{} is not quarter-sized!", words[2]);
                    std::process::exit(1);
                }
            }
            "quarter*" => {
                let size_alloc = (words.len() - 2) * 2;
                let name = words[1].to_owned();
                constants.insert(name, ptr_base as usize);
                ptr_base += 1;
                inter_insts.push(HandleAsm::Other(size_alloc as u64)); // li a <size>
                inter_insts.push(HandleAsm::Other(((get_num_reg_from_asm("b") as u64) << 54) + ptr_base as u64)); // li b <new_ptr>
                inter_insts.push(HandleAsm::Other(126100789566373898)); // li h 10
                inter_insts.push(HandleAsm::Other(1873497444986126336)); // ncall
                inter_insts.push(HandleAsm::Other((5 << 61) + (3 << 58) + ((get_num_reg_from_asm("c") as u64) << 51))); // raw c a
                for i in 2..words.len() {
                    if let Ok(bite) = words[i].parse::<u16>() {
                        inter_insts.push(HandleAsm::Other(18014398509481984 + bite as u64)); // li b <val>
                        inter_insts.push(HandleAsm::Other(902971725287784448)); // sq c b
                        inter_insts.push(HandleAsm::Other(2490490593935884290)); // addi c c 2
                    } else {
                        eprintln!("{} is not byte-sized!", words[i]);
                        std::process::exit(1);
                    }
                }

            }
            "half_u" => {
                if let Ok(val) = words[2].parse::<u32>() {
                    constants.insert(words[1].to_owned(), val as usize);
                } else {
                    eprintln!("{} is not half-sized!", words[2]);
                    std::process::exit(1);
                }
            }
            "half_u*" => {
                let size_alloc = (words.len() - 2) * 4;
                let name = words[1].to_owned();
                constants.insert(name, ptr_base as usize);
                ptr_base += 1;
                inter_insts.push(HandleAsm::Other(size_alloc as u64)); // li a <size>
                inter_insts.push(HandleAsm::Other(((get_num_reg_from_asm("b") as u64) << 54) + ptr_base as u64)); // li b <new_ptr>
                inter_insts.push(HandleAsm::Other(126100789566373898)); // li h 10
                inter_insts.push(HandleAsm::Other(1873497444986126336)); // ncall
                inter_insts.push(HandleAsm::Other((5 << 61) + (3 << 58) + ((get_num_reg_from_asm("c") as u64) << 51))); // raw c a
                for i in 2..words.len() {
                    if let Ok(bite) = words[i].parse::<u32>() {
                        inter_insts.push(HandleAsm::Other(18014398509481984 + (bite as u64 & 4_294_967_295))); // li b <val>
                        inter_insts.push(HandleAsm::Other(1047086913363640320)); // sh c b
                        inter_insts.push(HandleAsm::Other(2490490593935884292)); // addi c c 4
                    } else {
                        eprintln!("{} is not byte-sized!", words[i]);
                        std::process::exit(1);
                    }
                }

            }
            "half_i" => {
                if let Ok(val) = words[2].parse::<i32>() {
                    constants.insert(words[1].to_owned(), val as usize);
                } else {
                    eprintln!("{} is not half-sized!", words[2]);
                    std::process::exit(1);
                }
            }
            "half_i*" => {
                let size_alloc = (words.len() - 2) * 4;
                let name = words[1].to_owned();
                constants.insert(name, ptr_base as usize);
                ptr_base += 1;
                inter_insts.push(HandleAsm::Other(size_alloc as u64)); // li a <size>
                inter_insts.push(HandleAsm::Other(((get_num_reg_from_asm("b") as u64) << 54) + ptr_base as u64)); // li b <new_ptr>
                inter_insts.push(HandleAsm::Other(126100789566373898)); // li h 10
                inter_insts.push(HandleAsm::Other(1873497444986126336)); // ncall
                inter_insts.push(HandleAsm::Other((5 << 61) + (3 << 58) + ((get_num_reg_from_asm("c") as u64) << 51))); // raw c a
                for i in 2..words.len() {
                    if let Ok(bite) = words[i].parse::<i32>() {
                        inter_insts.push(HandleAsm::Other(18014398509481984 + (bite as u64 & 4_294_967_295))); // li b <val>
                        inter_insts.push(HandleAsm::Other(1047086913363640320)); // sh c b
                        inter_insts.push(HandleAsm::Other(2490490593935884292)); // addi c c 4
                    } else {
                        eprintln!("{} is not byte-sized!", words[i]);
                        std::process::exit(1);
                    }
                }

            }
            "word_u" => {
                if let Ok(val) = words[2].parse::<u64>() {
                    constants.insert(words[1].to_owned(), val as usize);
                } else {
                    eprintln!("{} is not word-sized!", words[2]);
                    std::process::exit(1);
                }
            }
            "word_u*" => {
                let size_alloc = (words.len() - 2) * 8;
                let name = words[1].to_owned();
                constants.insert(name, ptr_base as usize);
                ptr_base += 1;
                inter_insts.push(HandleAsm::Other(size_alloc as u64)); // li a <size>
                inter_insts.push(HandleAsm::Other(((get_num_reg_from_asm("b") as u64) << 54) + ptr_base as u64)); // li b <new_ptr>
                inter_insts.push(HandleAsm::Other(126100789566373898)); // li h 10
                inter_insts.push(HandleAsm::Other(1873497444986126336)); // ncall
                inter_insts.push(HandleAsm::Other((5 << 61) + (3 << 58) + ((get_num_reg_from_asm("c") as u64) << 51))); // raw c a
                inter_insts.push(HandleAsm::Other(32)); // li a 32
                for i in 2..words.len() {
                    if let Ok(bite) = words[i].parse::<u64>() {
                        let lsh = bite as u32;
                        let msh = bite >> 32;
                        inter_insts.push(HandleAsm::Other(18014398509481984 + (msh as u64 & 4_294_967_295))); // li b <val>                        
                        inter_insts.push(HandleAsm::Other(17314088767425871872)); // shl b b a
                        inter_insts.push(HandleAsm::Other(2470224395612717056 + (lsh as u64 & 4_294_967_295))); // addi b b <val>
                        inter_insts.push(HandleAsm::Other(1191202101439496192)); // sw c b
                        inter_insts.push(HandleAsm::Other(2490490593935884296)); // addi c c 8
                    } else {
                        eprintln!("{} is not byte-sized!", words[i]);
                        std::process::exit(1);
                    }
                }

            }
            "word_i" => {
                if let Ok(val) = words[2].parse::<i64>() {
                    constants.insert(words[1].to_owned(), val as usize);
                } else {
                    eprintln!("{} is not word-sized!", words[2]);
                    std::process::exit(1);
                }
            }
            "word_i*" => {
                let size_alloc = (words.len() - 2) * 8;
                let name = words[1].to_owned();
                constants.insert(name, ptr_base as usize);
                ptr_base += 1;
                inter_insts.push(HandleAsm::Other(size_alloc as u64)); // li a <size>
                inter_insts.push(HandleAsm::Other(((get_num_reg_from_asm("b") as u64) << 54) + ptr_base as u64)); // li b <new_ptr>
                inter_insts.push(HandleAsm::Other(126100789566373898)); // li h 10
                inter_insts.push(HandleAsm::Other(1873497444986126336)); // ncall
                inter_insts.push(HandleAsm::Other((5 << 61) + (3 << 58) + ((get_num_reg_from_asm("c") as u64) << 51))); // raw c a
                inter_insts.push(HandleAsm::Other(32)); // li a 32
                for i in 2..words.len() {
                    if let Ok(bite) = words[i].parse::<i64>() {
                        let lsh = bite as i32;
                        let msh = bite >> 32;
                        inter_insts.push(HandleAsm::Other(18014398509481984 + (msh as u64 & 4_294_967_295))); // li b <val>                        
                        inter_insts.push(HandleAsm::Other(17314088767425871872)); // shl b b a
                        inter_insts.push(HandleAsm::Other(2470224395612717056 + (lsh as u64 & 4_294_967_295))); // addi b b <val>
                        inter_insts.push(HandleAsm::Other(1191202101439496192)); // sw c b
                        inter_insts.push(HandleAsm::Other(2490490593935884296)); // addi c c 8
                    } else {
                        eprintln!("{} is not byte-sized!", words[i]);
                        std::process::exit(1);
                    }
                }

            }
            "asciistr" => {
                let mut flag = false;
                let mut vec_char = Vec::new();
                for ch in inst.trim().chars() {
                    if flag {
                        match ch {
                            'n' => {
                                if let Some(prev) = vec_char.pop() {
                                    if prev as char == '\\' {
                                        vec_char.push('\n' as u8);
                                    } else {
                                        vec_char.push(prev);
                                        vec_char.push('n' as u8);
                                    }
                                } else {
                                    vec_char.push(ch as u8);
                                }
                            }
                            _ => vec_char.push(ch as u8),
                        }
                        
                    }
                    if ch == '\"' {
                        flag = true;
                    }
                }
                vec_char.pop();
                let size_alloc = vec_char.len();
                let name = words[1].to_owned();
                constants.insert(name, ptr_base as usize);
                ptr_base += 1;
                inter_insts.push(HandleAsm::Other(size_alloc as u64)); // li a <size>
                inter_insts.push(HandleAsm::Other(((get_num_reg_from_asm("b") as u64) << 54) + ptr_base as u64)); // li b <new_ptr>
                inter_insts.push(HandleAsm::Other(126100789566373898)); // li h 10
                inter_insts.push(HandleAsm::Other(1873497444986126336)); // ncall
                inter_insts.push(HandleAsm::Other((5 << 61) + (3 << 58) + ((get_num_reg_from_asm("c") as u64) << 51))); // raw c a
                for val in vec_char.iter() {
                    inter_insts.push(HandleAsm::Other(18014398509481984 + *val as u64)); // li b <val>
                    inter_insts.push(HandleAsm::Other(758856537211928576)); // sb c b
                    inter_insts.push(HandleAsm::Other(2490490593935884289)); // addi c c 1
                }
            }
            // "str" => {
            //     let mut flag = false;
            //     let mut vec_char = Vec::new();
            //     for ch in inst.trim().chars() {
            //         if flag {
            //             vec_char.push(ch as u32);
            //         }
            //         if ch == '\"' {
            //             flag = true;
            //         }
            //     }
            //     vec_char.pop();
            //     let size_alloc = vec_char.len();
            //     let name = words[1].to_owned();
            //     constants.insert(name, ptr_base as usize);
            //     ptr_base += 1;
            //     inter_insts.push(HandleAsm::Other(size_alloc as u64)); // li a <size>
            //     inter_insts.push(HandleAsm::Other(126100789566373888)); // li h 0
            //     inter_insts.push(HandleAsm::Other(1873497444986126336)); // ncall
            //     inter_insts.push(HandleAsm::Other(2485986994308513792)); // addi c a 0
            //     for val in vec_char.iter() {
            //             inter_insts.push(HandleAsm::Other(18014398509481984 + (*val as u64 & 4_294_967_295))); // li b <val>
            //             inter_insts.push(HandleAsm::Other(1047086913363640320)); // sh c b
            //             inter_insts.push(HandleAsm::Other(2490490593935884292)); // addi c c 4
            //     }
            // }
            "label" => {
                labels.insert(words[1].to_owned(), inter_insts.len() - 1);       
            } 
            "comm"  => {}
            "li"    => {
                let dest = get_num_reg_from_asm(words[1]);
                let immv = words[2].parse::<i32>();
                match immv {
                    Ok(v) => {
                        let inst = ((dest as u64) << 54) + (v as u64 & 4_294_967_295); 
                        inter_insts.push(HandleAsm::Other(inst));
                    }
                    Err(_) => {
                        if let Some(addr) = labels.get(words[2]) {
                            let inst = ((dest as u64) << 54) + (*addr as u64 & 4_294_967_295);
                            inter_insts.push(HandleAsm::Other(inst as u64));
                        } else if let Some(addr) = constants.get(words[2]) {
                            let inst = ((dest as u64) << 54) + (*addr as u64 & 4_294_967_295);
                            inter_insts.push(HandleAsm::Other(inst as u64));
                        } else {
                            let inst = (dest as u64) << 54;
                            inter_insts.push(HandleAsm::UnresolvedJMP(inst, words[2].to_owned()))
                        }
                    }
                }
            }
            "lui"    => {
                let instrinsic_mask = (14 as u64) << 57;
                let dest = get_num_reg_from_asm(words[1]);
                let immv = words[2].parse::<i32>();
                match immv {
                    Ok(v) => {
                        let inst = instrinsic_mask + ((dest as u64) << 54) + (v as u64 & 4_294_967_295); 
                        inter_insts.push(HandleAsm::Other(inst));
                    }
                    Err(_) => {
                        if let Some(addr) = labels.get(words[2]) {
                            let inst = ((dest as u64) << 54) + (*addr as u64 & 4_294_967_295);
                            inter_insts.push(HandleAsm::Other(inst as u64));
                        } else if let Some(addr) = constants.get(words[2]) {
                            let inst = ((dest as u64) << 54) + (*addr as u64 & 4_294_967_295);
                            inter_insts.push(HandleAsm::Other(inst as u64));
                        } else {
                            let inst = (dest as u64) << 54;
                            inter_insts.push(HandleAsm::UnresolvedJMP(inst, words[2].to_owned()))
                        }
                    }
                }
            }
            "lb"    => {
                let instrinsic_mask = (1 as u64) << 57;
                let dest = get_num_reg_from_asm(words[1]);
                let src = words[2].parse::<u32>();
                match src {
                    Ok(v) => {
                        if words.len() == 4 {
                            let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64 + offset as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                    }
                    Err(_) => {
                        if avail_regs.contains(&words[2]) {
                            if words.len() == 4 {
                                let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                                let inst = instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                let inst = instrinsic_mask + ((dest as u64) << 54) + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst))
                            }
                        } else {
                            eprintln!("{} is not a valid addr!", words[2]);
                            std::process::exit(1);
                        }
                    }
                }
            }
            "raw"    => {
                let instrinsic_mask = (5 << 61) + (3 << 58);
                let dest = get_num_reg_from_asm(words[1]);
                let src = words[2].parse::<u32>();
                match src {
                    Ok(v) => {
                        if words.len() == 4 {
                            let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64 + offset as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                    }
                    Err(_) => {
                        if avail_regs.contains(&words[2]) {
                            if words.len() == 4 {
                                let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                                let inst = instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                let inst = instrinsic_mask + ((dest as u64) << 54) + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst))
                            }
                        } else {
                            eprintln!("{} is not a valid addr!", words[2]);
                            std::process::exit(1);
                        }
                    }
                }
            }
            "lq"    => {
                let instrinsic_mask = (2 as u64) << 57;
                let dest = get_num_reg_from_asm(words[1]);
                let src = words[2].parse::<u32>();
                match src {
                    Ok(v) => {
                        if words.len() == 4 {
                            let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64 + offset as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                    }
                    Err(_) => {
                        if avail_regs.contains(&words[2]) {
                            if words.len() == 4 {
                                let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                                let inst = instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                let inst = instrinsic_mask + ((dest as u64) << 54) + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst))
                            }
                        } else {
                            eprintln!("{} is not a valid addr!", words[2]);
                            std::process::exit(1);
                        }
                    }
                }
            }
            "lh"    => {
                let instrinsic_mask = (3 as u64) << 57;
                let dest = get_num_reg_from_asm(words[1]);
                let src = words[2].parse::<u32>();
                match src {
                    Ok(v) => {
                        if words.len() == 4 {
                            let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64 + offset as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                    }
                    Err(_) => {
                        if avail_regs.contains(&words[2]) {
                            if words.len() == 4 {
                                let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                                let inst = instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                let inst = instrinsic_mask + ((dest as u64) << 54) + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst))
                            }
                        } else {
                            eprintln!("{} is not a valid addr!", words[2]);
                            std::process::exit(1);
                        }
                    }
                }
            }
            "lw"    => {
                let instrinsic_mask = (4 as u64) << 57;
                let dest = get_num_reg_from_asm(words[1]);
                let src = words[2].parse::<u32>();
                match src {
                    Ok(v) => {
                        if words.len() == 4 {
                            let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64 + offset as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                    }
                    Err(_) => {
                        if avail_regs.contains(&words[2]) {
                            if words.len() == 4 {
                                let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                                let inst = instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                let inst = instrinsic_mask + ((dest as u64) << 54) + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst))
                            }
                        } else {
                            eprintln!("{} is not a valid addr!", words[2]);
                            std::process::exit(1);
                        }
                    }
                }
            }
            "sb"    => {
                let instrinsic_mask = (5 as u64) << 57;
                let dest = get_num_reg_from_asm(words[1]);
                let src = words[2].parse::<u32>();
                match src {
                    Ok(v) => {
                        if words.len() == 4 {
                            let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64 + offset as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                    }
                    Err(_) => {
                        if avail_regs.contains(&words[2]) {
                            if words.len() == 4 {
                                let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                                let inst = instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                let inst = instrinsic_mask + ((dest as u64) << 54) + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst))
                            }
                        } else {
                            eprintln!("{} is not a valid addr!", words[2]);
                            std::process::exit(1);
                        }
                    }
                }
            }
            "sq"    => {
                let instrinsic_mask = (6 as u64) << 57;
                let dest = get_num_reg_from_asm(words[1]);
                let src = words[2].parse::<u32>();
                match src {
                    Ok(v) => {
                        if words.len() == 4 {
                            let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64 + offset as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                    }
                    Err(_) => {
                        if avail_regs.contains(&words[2]) {
                            if words.len() == 4 {
                                let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                                let inst = instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                let inst = instrinsic_mask + ((dest as u64) << 54) + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst))
                            }
                        } else {
                            eprintln!("{} is not a valid addr!", words[2]);
                            std::process::exit(1);
                        }
                    }
                }
            }
            "sh"    => {
                let instrinsic_mask = (7 as u64) << 57;
                let dest = get_num_reg_from_asm(words[1]);
                let src = words[2].parse::<u32>();
                match src {
                    Ok(v) => {
                        if words.len() == 4 {
                            let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64 + offset as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                    }
                    Err(_) => {
                        if avail_regs.contains(&words[2]) {
                            if words.len() == 4 {
                                let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                                let inst = instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                let inst = instrinsic_mask + ((dest as u64) << 54) + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst))
                            }
                        } else {
                            eprintln!("{} is not a valid addr!", words[2]);
                            std::process::exit(1);
                        }
                    }
                }
            }
            "sw"    => {
                let instrinsic_mask = (8 as u64) << 57;
                let dest = get_num_reg_from_asm(words[1]);
                let src = words[2].parse::<u32>();
                match src {
                    Ok(v) => {
                        if words.len() == 4 {
                            let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64 + offset as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                    }
                    Err(_) => {
                        if avail_regs.contains(&words[2]) {
                            if words.len() == 4 {
                                let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                                let inst = instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                let inst = instrinsic_mask + ((dest as u64) << 54) + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst))
                            }
                        } else {
                            eprintln!("{} is not a valid addr!", words[2]);
                            std::process::exit(1);
                        }
                    }
                }
            } 
            "pop" => {
                let instrinsic_mask = (9 as u64) << 57;
                let dest = get_num_reg_from_asm(words[1]);
                let inst = instrinsic_mask + ((dest as u64) << 54);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "push" => {
                let instrinsic_mask = (10 as u64) << 57;
                let src1 = get_num_reg_from_asm(words[1]);
                let inst = instrinsic_mask + ((src1 as u64) << 51);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "ls" => {
                let instrinsic_mask = (11 as u64) << 57;
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let inst = instrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "ss" => {
                let instrinsic_mask = (12 as u64) << 57;
                let src1 = get_num_reg_from_asm(words[1]);
                let src2 = get_num_reg_from_asm(words[2]);
                let inst = instrinsic_mask + ((src2 as u64) << 48) + ((src1 as u64) << 51);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "ncall" => {
                let instrinsic_mask = 13u64 << 57;
                inter_insts.push(HandleAsm::Other(instrinsic_mask));
            }
            "add" => {
                let intrinsic_mask = (1u64 << 61) + (0 << 58) + (0 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "addi" => {
                let intrinsic_mask = (1u64 << 61) + (0 << 58) + (1 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                if let Ok(imm) =  words[3].parse::<i64>() {
                    let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + (imm as u64 & 4_294_967_295);
                    inter_insts.push(HandleAsm::Other(inst));
                } else {
                    let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                    inter_insts.push(HandleAsm::UnresolvedJMP(inst, words[3].to_owned()));
                }
            }
            "addf" => {
                let intrinsic_mask = (2u64 << 61) + (0 << 58) + (1 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "sub" => {
                let intrinsic_mask = (1u64 << 61) + (1 << 58) + (0 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "subi" => {
                let intrinsic_mask = (1u64 << 61) + (1 << 58) + (1 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                if let Ok(imm) =  words[3].parse::<i64>() {
                    let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + (imm as u64 & 4_294_967_295);
                    inter_insts.push(HandleAsm::Other(inst));
                } else {
                    let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                    inter_insts.push(HandleAsm::UnresolvedJMP(inst, words[3].to_owned()));
                }            
            }
            "subf" => {
                let intrinsic_mask = (2u64 << 61) + (1 << 58) + (1 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            } 
            "mul" => {
                let intrinsic_mask = (1u64 << 61) + (2 << 58) + (0 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "muli" => {
                let intrinsic_mask = (1u64 << 61) + (2 << 58) + (1 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                if let Ok(imm) =  words[3].parse::<i64>() {
                    let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + (imm as u64 & 4_294_967_295);
                    inter_insts.push(HandleAsm::Other(inst));
                } else {
                    let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                    inter_insts.push(HandleAsm::UnresolvedJMP(inst, words[3].to_owned()));
                }
            }
            "mulf" => {
                let intrinsic_mask = (2u64 << 61) + (2 << 58) + (1 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "div" => {
                let intrinsic_mask = (1u64 << 61) + (3 << 58) + (0 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "divi" => {
                let intrinsic_mask = (1u64 << 61) + (3 << 58) + (1 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                if let Ok(imm) =  words[3].parse::<i64>() {
                    let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + (imm as u64 & 4_294_967_295);
                    inter_insts.push(HandleAsm::Other(inst));
                } else {
                    let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                    inter_insts.push(HandleAsm::UnresolvedJMP(inst, words[3].to_owned()));
                }
            }
            "divf" => {
                let intrinsic_mask = (2u64 << 61) + (3 << 58) + (1 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            } 
            "mod" => {
                let intrinsic_mask = (1u64 << 61) + (4 << 58) + (0 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "modi" => {
                let intrinsic_mask = (1u64 << 61) + (4 << 58) + (1 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                if let Ok(imm) =  words[3].parse::<i64>() {
                    let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + (imm as u64 & 4_294_967_295);
                    inter_insts.push(HandleAsm::Other(inst));
                } else {
                    let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                    inter_insts.push(HandleAsm::UnresolvedJMP(inst, words[3].to_owned()));
                }
            }
            "modf" => {
                let intrinsic_mask = (2u64 << 61) + (4 << 58) + (1 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "itof_32" => {
                let instrinsic_mask = (3u64 << 61) + (0 << 58);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let inst = instrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "itof_64" => {
                let instrinsic_mask = (3u64 << 61) + (1 << 58);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let inst = instrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "ftoi_32" => {
                let instrinsic_mask = (3u64 << 61) + (2 << 58);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let inst = instrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "ftoi_64" => {
                let instrinsic_mask = (3u64 << 61) + (3 << 58);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let inst = instrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "eq_u" => {
                let instrinsic_mask = (4u64 << 61) + (0 << 58);
                let src1 = get_num_reg_from_asm(words[1]);
                let src2 = get_num_reg_from_asm(words[2]);
                let inst = instrinsic_mask + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "cmp_u" => {
                let instrinsic_mask = (4u64 << 61) + (1 << 58);
                let src1 = get_num_reg_from_asm(words[1]);
                let src2 = get_num_reg_from_asm(words[2]);
                let inst = instrinsic_mask + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "ez_u" => {
                let instrinsic_mask = (4u64 << 61) + (2 << 58);
                let src1 = get_num_reg_from_asm(words[1]);
                let inst = instrinsic_mask + ((src1 as u64) << 51);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "eq_i" => {
                let instrinsic_mask = (4u64 << 61) + (1 << 57);
                let src1 = get_num_reg_from_asm(words[1]);
                let src2 = get_num_reg_from_asm(words[2]);
                let inst = instrinsic_mask + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "cmp_i" => {
                let instrinsic_mask = (4u64 << 61) + (3 << 57);
                let src1 = get_num_reg_from_asm(words[1]);
                let src2 = get_num_reg_from_asm(words[2]);
                let inst = instrinsic_mask + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "ez_i" => {
                let instrinsic_mask = (4u64 << 61) + (5 << 57);
                let src1 = get_num_reg_from_asm(words[1]);
                let inst = instrinsic_mask + ((src1 as u64) << 51);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "size"    => {
                let instrinsic_mask = (4u64 << 61) + (6 << 57);
                let dest = get_num_reg_from_asm(words[1]);
                let src = words[2].parse::<u32>();
                match src {
                    Ok(v) => {
                        if words.len() == 4 {
                            let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64 + offset as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            let inst = instrinsic_mask + ((dest as u64) << 54) + v as u64;
                            inter_insts.push(HandleAsm::Other(inst));
                        }
                    }
                    Err(_) => {
                        if avail_regs.contains(&words[2]) {
                            if words.len() == 4 {
                                let offset = words[3].parse::<u32>().expect(&format!("{} is not a valid offset!",words[3]));
                                let inst = instrinsic_mask + ((dest as u64) << 54) + offset as u64;
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                let inst = instrinsic_mask + ((dest as u64) << 54) + ((get_num_reg_from_asm(words[2]) as u64) << 51);
                                inter_insts.push(HandleAsm::Other(inst))
                            }
                        } else {
                            eprintln!("{} is not a valid addr!", words[2]);
                            std::process::exit(1);
                        }
                    }
                }
            }
            "eq_f" => {
                let instrinsic_mask = (5u64 << 61) + (0 << 58);
                let src1 = get_num_reg_from_asm(words[1]);
                let src2 = get_num_reg_from_asm(words[2]);
                let inst = instrinsic_mask + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "cmp_f" => {
                let instrinsic_mask = (5u64 << 61) + (1 << 58);
                let src1 = get_num_reg_from_asm(words[1]);
                let src2 = get_num_reg_from_asm(words[2]);
                let inst = instrinsic_mask + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "ez_f" => {
                let instrinsic_mask = (5u64 << 61) + (2 << 58);
                let src1 = get_num_reg_from_asm(words[1]);
                let inst = instrinsic_mask + ((src1 as u64) << 51);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "hlt"  => {
                inter_insts.push(HandleAsm::Other(INST_HALT_VM));
            }
            "jmp" => {
                let instrinsic_mask = (6u64 <<61) + (0<<58);
                let addr = words[1];
                match addr.parse::<u32>() {
                    Ok(v) => {
                        let inst = instrinsic_mask + v as u64;
                        inter_insts.push(HandleAsm::Other(inst));
                    }
                    Err(_) => {
                        if avail_regs.contains(&addr) {
                            let src1 = get_num_reg_from_asm(addr);
                            let inst = instrinsic_mask + ((src1 as u64) << 51);
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            if let Some(to_jmp) = labels.get(addr) {
                                let inst = instrinsic_mask + (*to_jmp as u64);
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                inter_insts.push(HandleAsm::UnresolvedJMP(instrinsic_mask, addr.to_owned()));
                            }
                        }
                    }
                }
            }
            "jez" => {
                let instrinsic_mask = (6u64 <<61) + (1<<58);
                let addr = words[1];
                match addr.parse::<u32>() {
                    Ok(v) => {
                        let inst = instrinsic_mask + v as u64;
                        inter_insts.push(HandleAsm::Other(inst));
                    }
                    Err(_) => {
                        if avail_regs.contains(&addr) {
                            let src1 = get_num_reg_from_asm(addr);
                            let inst = instrinsic_mask + ((src1 as u64) << 51);
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            if let Some(to_jmp) = labels.get(addr) {
                                let inst = instrinsic_mask + (*to_jmp as u64);
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                inter_insts.push(HandleAsm::UnresolvedJMP(instrinsic_mask, addr.to_owned()));
                            }
                        }
                    }
                }
            }
            "jnz" => {
                let instrinsic_mask = (6u64 <<61) + (2<<58);
                let addr = words[1];
                match addr.parse::<u32>() {
                    Ok(v) => {
                        let inst = instrinsic_mask + v as u64;
                        inter_insts.push(HandleAsm::Other(inst));
                    }
                    Err(_) => {
                        if avail_regs.contains(&addr) {
                            let src1 = get_num_reg_from_asm(addr);
                            let inst = instrinsic_mask + ((src1 as u64) << 51);
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            if let Some(to_jmp) = labels.get(addr) {
                                let inst = instrinsic_mask + (*to_jmp as u64);
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                inter_insts.push(HandleAsm::UnresolvedJMP(instrinsic_mask, addr.to_owned()));
                            }
                        }
                    }
                }
            }
            "jge" => {
                let instrinsic_mask = (6u64 <<61) + (3<<58);
                let addr = words[1];
                match addr.parse::<u32>() {
                    Ok(v) => {
                        let inst = instrinsic_mask + v as u64;
                        inter_insts.push(HandleAsm::Other(inst));
                    }
                    Err(_) => {
                        if avail_regs.contains(&addr) {
                            let src1 = get_num_reg_from_asm(addr);
                            let inst = instrinsic_mask + ((src1 as u64) << 51);
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            if let Some(to_jmp) = labels.get(addr) {
                                let inst = instrinsic_mask + (*to_jmp as u64);
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                inter_insts.push(HandleAsm::UnresolvedJMP(instrinsic_mask, addr.to_owned()));
                            }
                        }
                    }
                }
            }
            "jlt" => {
                let instrinsic_mask = (6u64 << 61) + (4<<58);
                let addr = words[1];
                match addr.parse::<u32>() {
                    Ok(v) => {
                        let inst = instrinsic_mask + v as u64;
                        inter_insts.push(HandleAsm::Other(inst));
                    }
                    Err(_) => {
                        if avail_regs.contains(&addr) {
                            let src1 = get_num_reg_from_asm(addr);
                            let inst = instrinsic_mask + ((src1 as u64) << 51);
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            if let Some(to_jmp) = labels.get(addr) {
                                let inst = instrinsic_mask + (*to_jmp as u64);
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                inter_insts.push(HandleAsm::UnresolvedJMP(instrinsic_mask, addr.to_owned()));
                            }
                        }
                    }
                }
            }
            "call" => {
                let instrinsic_mask = (6u64 << 61) + (5<<58);
                let addr = words[1];
                match addr.parse::<u32>() {
                    Ok(v) => {
                        let inst = instrinsic_mask + v as u64;
                        inter_insts.push(HandleAsm::Other(inst));
                    }
                    Err(_) => {
                        if avail_regs.contains(&addr) {
                            let src1 = get_num_reg_from_asm(addr);
                            let inst = instrinsic_mask + ((src1 as u64) << 51);
                            inter_insts.push(HandleAsm::Other(inst));
                        } else {
                            if let Some(to_jmp) = labels.get(addr) {
                                let inst = instrinsic_mask + (*to_jmp as u64);
                                inter_insts.push(HandleAsm::Other(inst));
                            } else {
                                inter_insts.push(HandleAsm::UnresolvedJMP(instrinsic_mask, addr.to_owned()));
                            }
                        }
                    }
                }
            }
            "ret" => {
                let instrinsic_mask = (6u64 << 61) + (7 << 58);
                inter_insts.push(HandleAsm::Other(instrinsic_mask));    
            }
            "and" => {
                let intrinsic_mask = (7u64 << 61) + (0 << 58);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "or" => {
                let intrinsic_mask = (7u64 << 61) + (1 << 58);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "xor" => {
                let intrinsic_mask = (7u64 << 61) + (2 << 58);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "shl" => {
                let intrinsic_mask = (7u64 << 61) + (4 << 58);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            "shr" => {
                let intrinsic_mask = (7u64 << 61) + (5 << 58);
                let dest = get_num_reg_from_asm(words[1]);
                let src1 = get_num_reg_from_asm(words[2]);
                let src2 = get_num_reg_from_asm(words[3]);
                let inst = intrinsic_mask + ((dest as u64) << 54) + ((src1 as u64) << 51) + ((src2 as u64) << 48);
                inter_insts.push(HandleAsm::Other(inst));
            }
            
            a => panic!("Not implemented: {}",a),
        }
    }
    //let insts: Vec<u64> = inter_insts.into_iter().map(HandleAsm::unwrap).collect();
    let mut insts: Vec<u64> = vec![];
    for inst in inter_insts {
        match inst {
            HandleAsm::Other(a) => insts.push(a),
            HandleAsm::UnresolvedJMP(a, addr) => {
                if let Some(to_jmp) = labels.get(&addr) {
                    let inst = a + (*to_jmp as u64);
                    insts.push(inst);
                } else if let Some(cons) = constants.get(&addr) {
                    insts.push(a + *cons as u64);
                } else {
                    eprintln!("Undefined Label: {}",addr);
                    std::process::exit(1);
                }
            }
        }
    }
    for elem in insts {
        let bytes = elem.to_le_bytes();
        for byte in bytes {
            ret.push(byte)
        }
    }
    ret
}



fn read_asm_file(file: &str) -> String {
    let f = std::fs::read_to_string(file);
    match f {
        Ok(s) => return s,
        Err(e) => panic!("{}",e),
    }
}

fn write_bytecode(bc: Vec<u8>, file: &str) {
    let mut f = std::fs::OpenOptions::new().write(true).create(true).truncate(true).open(file).expect("Unable to open file");
    match f.write_all(bc.as_ref()) {
        Ok(_) => {}
        Err(e) => panic!("{e}"),
    }
}

fn bc_to_inst(bc: Vec<u8>) -> Vec<u64> {
    let mut ret = Vec::new();
    let mut arr = [0u8;8];
    for bytes in bc.chunks(8) {
        if bytes.len() == 8 {
            let mut tmp = bytes.to_vec();
            tmp.resize(8, 0);
            for (i,byte) in tmp.into_iter().enumerate() {
                arr[i] = byte;
            }
            ret.push(u64::from_le_bytes(arr))
        }
    }
    ret
}

fn assemble_to_bc(filename: &str, asm: String) {
    let final_name = filename.to_owned() + ".mknc";
    let mut included_files = Vec::new();
    let bytecode = asm_to_bytecode(asm, &mut included_files);
    write_bytecode(bytecode, &final_name);
}

fn run_from_bc(filename: &str) {
    match std::fs::read(filename) {
        Ok(f) =>  {
            let program = bc_to_inst(f);
            let mut vm = VM::new();
            vm.load_nice_natives();
            vm.load_prog(program);
            vm.execute();
        }
        Err(_e) => {
            eprintln!("Error: unable to open file {}.", filename);
            std::process::exit(1);
        }
    }
    
}

fn main() {
    //let mut vm = VM::new();
    //vm.load_nice_natives();
    //println!("{}",&vm);
    //let inst_one = 0b000_0000_000_000_000_0000000000000000_00000000000000000000000000000011;
    //let inst_two = 0b000_0000_001_000_000_0000000000000000_00000000000000000000000000000101;
    //let program = vec![inst_one,inst_two,INST_HALT_VM];
    //let contents = read_asm_file("test.asm");
    //println!("CONTENTS: {}\n", &contents);
    //let bytecode = asm_to_bytecode(contents);
    //println!("BYTECODE: {:?}\n", &bytecode);
    //let program  = bc_to_inst(bytecode);
    //println!("PROGRAM: {:?}", program);
    //vm.load_prog(program);
    //vm.execute();
    //println!("{}",&vm);
    let args: Vec<String> = std::env::args().collect();
    //println!("{:?}",args);
    match args.len() {
        1 => {
            println!("mknb <-c (compile)/-r (run)> <file>");
            println!("Source asm files:     <name>.mknasm");
            println!("Bytecode files:       <name>.mknc");
        }
        2 => {
            println!("mknb <-c (compile)/-r (run)> <file>");
            println!("Source asm files:     <name>.mknasm");
            println!("Bytecode files:       <name>.mknc");
        }
        3 => {
            match args[1].as_ref() {
                "-c" => {
                    let filename = args[2].split(".").collect::<Vec<&str>>()[0];
                    let asm = read_asm_file(&args[2]);
                    assemble_to_bc(filename, asm);
                }
                "-r" => {
                    run_from_bc(&args[2]);

                }
                e => {
                    eprintln!("Unrecognised option {}",e);
                    std::process::exit(1);
                }
            }
        }
        _ => {
            println!("mknb <-c (compile)/-r (run)> <file>");
            println!("Source asm files:     <name>.mknasm");
            println!("Bytecode files:       <name>.mknc");
        } 
    }
}
